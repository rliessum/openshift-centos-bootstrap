Install RedHat OpenShift Origin in your development box.

- Installation

1. Create a VM as explained in [[http://www.youtube.com/watch?v=-OOnGK-XeVY][this video]] by Grant Shipley
2. Define the domain name, user name and password defined at installation time of Centos7.

```
export DOMAIN=mycompany.com
export USERNAME=user
export PASSWORD=yourpassword
```

1. Run the automagic installation script as root:

```
curl https://gitlab.com/rliessum/openshift-centos-bootstrap/raw/master/install-openshift.sh | /bin/bash
```

This script contains some personal modifications because some things didn't work out for me.

Majority of the source comes from https://github.com/gshipley/installcentos